from tkinter import *

"""-----------------------------------------INIT------------------------------------------------"""

# create the window
window = Tk()

# create frames where we can put buttons and labels
frame0 = Frame(window, bg='#439EE4')
frame1 = Frame(window, bg='#439EE4', bd=1, relief=SUNKEN)
frame2 = Frame(window, bg='#439EE4')
frame3 = Frame(window, bg='#439EE4')
frameTitle0 = Frame(window, bg='#439EE4')
frameTitle1 = Frame(window, bg='#439EE4')

# initialise the window
window.title("Han- man")
window.geometry("400x750")
window.config(background='#439EE4')
window.iconbitmap("hangman (1).ico")
window.wm_minsize(width=350, height=700)
window.wm_maxsize(width=500, height=800)

# title
label_title = Label(frameTitle0, text="Welcome to\n The Hangman Game", font=("courrier", 25), bg='#439EE4', fg='white')
label_title.pack(side=TOP)

# Loading The image
width = 200
height = 200
Image = PhotoImage(file="Hangman-icon.png").zoom(23).subsample(30)
canvas = Canvas(window, width=width, height=height)
canvas.create_image(width / 2, height / 2, image=Image)


def recherche(cara, full):
    i = 0
    I = []
    J = []
    for i in range(len(full)):
        if cara == full[i] and full.count(full[i]) == 1:
            I.append(i)
            return I
        elif cara == full[i] and full.count(full[i]) > 1:
            I.append(i)
    if len(I) != 0:
        return I
    J.append(-1)
    return J


def remplacer(se, ca, i):
    semi1 = se[:i]
    semi2 = se[i + 1:]
    semi1 = semi1 + ca
    # semi2 = semi2 - semi2[0]
    se = semi1 + semi2
    return se


def saisie(caracteresaisie):
    global c
    c = caracteresaisie


def game():
    global c
    ful = "Archery"
    sem = "A------"
    frame1.pack_forget()
    canvas.pack_forget()
    frameTitle0.pack_forget()
    label_title2 = Label(frameTitle1, text="Guess The Word", font=("courrier", 25), bg='#439EE4', fg='white')
    label_title2.pack(side=TOP)
    label_title3 = Label(frameTitle1, text="It's a Sport Game !! ", font=("courrier", 20), bg='#439EE4', fg='orange')
    label_title3.pack(side=TOP)
    print(c)
    #the problem is here  
    while sem != ful and c != "INIT":
        print(c)
        index = []
        j = 0
        mot = Label(frameTitle1, text=sem, font=("courrier", 30), bg='#439EE4', fg='black')
        mot.pack(expand=YES)
        index = recherche(c, ful)
        if index[0] == -1:
            print("It's not the right letter")
            mot = Label(frameTitle1, text=sem, font=("courrier", 30), bg='#439EE4', fg='black')
            mot.pack(expand=YES)

        else:
            for j in range(len(index)):
                sem = remplacer(sem, c, index[j])
        c = "INIT"
    print("Great Job")
    w = input("to know more about " + sem + " click button w")
    if w == 'w':
        print("Archery is the art, sport, practice, or skill of using a bow to shoot arrows.[1] The word comes from "
              "the Latin arcus. Historically, archery has been used for hunting and combat. In modern times, "
              "it is mainly a competitive sport and recreational activity. A person who participates in archery is "
              "typically called an archer or a bowman, and a person who is fond of or an expert at archery is "
              "sometimes called a toxophilite.")


# create a button

"""-------------------------------------Lblan dyal Start Game ----------------------------------------------"""

start_button = Button(frame1, text="STA-T  GA-E", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
start_button1 = Button(frame1, text="START  GA-E", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
start_button2 = Button(frame1, text="START  GAME", font=("Baskerville Old Face", 25), bg='orange', fg='white',
                       command=game)
start_button3 = Button(frame1, text="STA-T  GAME", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
start_button.pack()
a = 0
b = 0


def newTitle1():
    global a
    global b
    if a == 1:
        start_button3.pack_forget()
        start_button2.pack()
    else:
        start_button.pack_forget()
        start_button1.pack()
        b = 1


def newTitle2():
    global a
    global b
    if b == 1:
        start_button1.pack_forget()
        start_button2.pack()
    else:
        start_button.pack_forget()
        start_button3.pack()
        a = 1


"""--------------------------------------ABCDEFGIJKLMNOPQRSTUVWXYZ-------------------------------------------------"""

A = Button(frame2, text="A", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4', command=saisie('A'))
A.grid(row=0, column=0)
# frame2.bind("<>", saisie)
B = Button(frame2, text="B", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4', )
B.grid(row=0, column=1)
C = Button(frame2, text="C", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4', )
C.grid(row=0, column=2)
D = Button(frame2, text="D", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
D.grid(row=0, column=3)
E = Button(frame2, text="E", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
E.grid(row=0, column=4)
F = Button(frame2, text="F ", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
F.grid(row=1, column=0)
G = Button(frame2, text="G", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
G.grid(row=1, column=1)
H = Button(frame2, text="H", font=("Baskerville Old Face", 24), bg='white', fg='#439EE4')
H.grid(row=1, column=2)
I = Button(frame2, text=" I ", font=("Baskerville Old Face", 23), bg='white', fg='#439EE4')
I.grid(row=1, column=3)
J = Button(frame2, text=" J  ", font=("Baskerville Old Face", 22), bg='white', fg='#439EE4')
J.grid(row=1, column=4)
K = Button(frame2, text="K ", font=("Baskerville Old Face", 23), bg='white', fg='#439EE4')
K.grid(row=2, column=0)
L = Button(frame2, text="L ", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
L.grid(row=2, column=1)
M = Button(frame2, text="M", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4', command=newTitle2)
M.grid(row=2, column=2)
N = Button(frame2, text=" N", font=("Baskerville Old Face", 23), bg='white', fg='#439EE4')
N.grid(row=2, column=3)
O = Button(frame2, text="O ", font=("Baskerville Old Face", 22), bg='white', fg='#439EE4')
O.grid(row=2, column=4)
P = Button(frame2, text="P ", font=("Baskerville Old Face", 22), bg='white', fg='#439EE4')
P.grid(row=3, column=0)
Q = Button(frame2, text="Q", font=("Baskerville Old Face", 24), bg='white', fg='#439EE4')
Q.grid(row=3, column=1)
R = Button(frame2, text="R ", font=("Baskerville Old Face", 23), bg='white', fg='#439EE4', command=newTitle1)
R.grid(row=3, column=2)
S = Button(frame2, text="S ", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
S.grid(row=3, column=3)
T = Button(frame2, text="T", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
T.grid(row=3, column=4)
U = Button(frame2, text="U", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
U.grid(row=4, column=0)
V = Button(frame2, text="V", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
V.grid(row=4, column=1)
W = Button(frame2, text="W", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
W.grid(row=4, column=2)
X = Button(frame2, text="X", font=("Baskerville Old Face", 25), bg='white', fg='#439EE4')
X.grid(row=4, column=3)
Y = Button(frame2, text="Y", font=("Baskerville Old Face", 24), bg='white', fg='#439EE4')
Y.grid(row=4, column=4)
Z = Button(frame2, text="Z", font=("Baskerville Old Face", 21), bg='white', fg='#439EE4')
Z.grid(row=4, column=5)

frameTitle0.pack(side=TOP)
canvas.pack(expand=YES)
frameTitle1.pack(side=TOP)
# frame0.pack(expand=YES)
frame1.pack(expand=YES)
frame2.pack(side=BOTTOM)
frame3.pack(side=LEFT)

# print the window
window.mainloop()
